<?php

namespace Drupal\social_auth_vipps\Provider;

use Drupal\social_auth_vipps\OptionProvider\VippsAuthOptionProvider;
use Drupal\social_auth_vipps\Provider\Exception\VippsIdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;
use League\OAuth2\Client\Provider\AbstractProvider;

/**
 * Class Vipps provider.
 *
 * @package Drupal\social_auth_vipps\Provider
 */
class Vipps extends AbstractProvider {
  use BearerAuthorizationTrait;

  /**
   * Is test mode.
   *
   * @var bool
   */
  protected $testMode = FALSE;

  /**
   * Vipps constructor.
   *
   * @param array $options
   *   Options array.
   * @param array $collaborators
   *   Collaborators array.
   */
  public function __construct(array $options = [], array $collaborators = []) {
    parent::__construct($options, $collaborators);
    $this->setOptionProvider(new VippsAuthOptionProvider());
    $this->testMode = boolval($options['testMode']);
  }

  /**
   * Get authorization url to begin OAuth flow.
   *
   * @return string
   *   Authorization URL.
   */
  public function getBaseAuthorizationUrl() {
    return $this->getDomain() . '/access-management-1.0/access/oauth2/auth';
  }

  /**
   * Get access token url to retrieve token.
   *
   * @param array $params
   *   Parameters.
   *
   * @return string
   *   Base access token URL.
   */
  public function getBaseAccessTokenUrl(array $params) {
    return $this->getDomain() . '/access-management-1.0/access/oauth2/token';
  }

  /**
   * Get provider url to fetch user details.
   *
   * @param \League\OAuth2\Client\Token\AccessToken $token
   *   Access token.
   *
   * @return string
   *   resource owner details URL.
   */
  public function getResourceOwnerDetailsUrl(AccessToken $token) {
    return $this->getDomain() . '/access-management-1.0/access/userinfo';
  }

  /**
   * Get the default scopes used by this provider.
   *
   * This should not be a complete list of all scopes, but the minimum
   * required for the provider user interface!
   *
   * @return array
   *   Empty.
   */
  protected function getDefaultScopes() {
    return [];
  }

  /**
   * Check a provider response for errors.
   * @link https://developer.github.com/v3/#client-errors
   * @link https://developer.github.com/v3/oauth/#common-errors-for-the-access-token-request
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response.
   * @param string $data
   *   Parsed response data.
   *
   * @throws \Drupal\social_auth_vipps\Provider\Exception\VippsIdentityProviderException
   */
  protected function checkResponse(ResponseInterface $response, $data) {
    if ($response->getStatusCode() >= 400) {
      throw VippsIdentityProviderException::clientException($response, $data);
    }
    elseif (isset($data['error'])) {
      throw VippsIdentityProviderException::oauthException($response, $data);
    }
  }

  /**
   * Generate a user object from a successful user details request.
   *
   * @param array $response
   *   Response.
   * @param \League\OAuth2\Client\Token\AccessToken $token
   *   Access token.
   *
   * @return \League\OAuth2\Client\Provider\ResourceOwnerInterface
   *   Resource owner.
   */
  protected function createResourceOwner(array $response, AccessToken $token) {
    $user = new VippsResourceOwner($response);

    return $user->setDomain($this->getDomain());
  }

  /**
   * Get scope separator.
   *
   * @return string
   *   String +.
   */
  protected function getScopeSeparator() {
    return '+';
  }

  /**
   * Build a query string from array. Vipps API doesn't work with encoded url.
   *
   * @param array $params
   *   Parameters.
   *
   * @return string
   *   Url decoded.
   */
  protected function buildQueryString(array $params) {
    return urldecode(parent::buildQueryString($params));
  }

  /**
   * Check is test.
   *
   * @return bool
   *   True or false.
   */
  protected function isTest() {
    return $this->testMode;
  }

  /**
   * Get Vipps domain.
   *
   * @return string
   *   Vipps domain.
   */
  protected function getDomain() {
    return sprintf("https://%s.vipps.no", $this->isTest() ? 'apitest' : 'api');
  }

}
