<?php

namespace Drupal\social_auth_vipps\Provider\Exception;

use Psr\Http\Message\ResponseInterface;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;

/**
 * Class Vipps Identity Provider Exception.
 *
 * @package Drupal\social_auth_vipps\Provider\Exception
 */
class VippsIdentityProviderException extends IdentityProviderException {

  /**
   * Creates client exception from response.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response.
   * @param string $data
   *   Parsed response data.
   *
   * @return VippsIdentityProviderException
   *   Vipps identidy provider exception.
   */
  public static function clientException(ResponseInterface $response, $data) {
    return static::fromResponse(
      $response,
      isset($data['message']) ? $data['message'] : $response->getReasonPhrase()
    );
  }

  /**
   * Creates oauth exception from response.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response.
   * @param string $data
   *   Parsed response data.
   *
   * @return VippsIdentityProviderException
   *   Vipps identidy provider exception.
   */
  public static function oauthException(ResponseInterface $response, string $data) {
    return static::fromResponse(
      $response,
      isset($data['error']) ? $data['error'] : $response->getReasonPhrase()
    );
  }

  /**
   * Creates identity exception from response.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response.
   * @param string $message
   *   Message.
   *
   * @return VippsIdentityProviderException
   *   Vipps identidy provider exception.
   */
  protected static function fromResponse(ResponseInterface $response, $message = NULL) {
    return new static($message, $response->getStatusCode(), (string) $response->getBody());
  }

}
