<?php

namespace Drupal\social_auth_vipps\Provider\Exception;

/**
 * Class Email Not Verified Exception.
 *
 * @package Drupal\social_auth_vipps\Provider\Exception
 */
class EmailNotVerifiedException extends \Exception {

}
